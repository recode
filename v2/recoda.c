#include <complex.h>
#include <math.h>
#include <string.h>
#include <fftw3.h>
#include <sndfile.h>

#define twopi 6.283185307179586

#define CHANNELS 2
#define OCTAVES 11
#define FFTSIZE (1 << OCTAVES)
#define OVERLAP 4

inline float cnormf(float _Complex f) {
  return crealf(f) * crealf(f) + cimagf(f) * cimagf(f);
}

int main(int argc, char **argv) {

  if (argc != 4) {
    fprintf(stderr, "usage: %s source.wav control.wav out.wav\n", argv[0]);
    return 1;
  }

  float *window = fftwf_malloc(sizeof(*window) * FFTSIZE);
  for (int i = 0; i < FFTSIZE; ++i) {
    window[i] = 0.5 * (1 - cos(i * twopi / FFTSIZE)); 
  }
  float *fft_in = fftwf_malloc(sizeof(*fft_in) * FFTSIZE);
  float _Complex *fft_out = fftwf_malloc(sizeof(*fft_out) * FFTSIZE * 2);
  fftwf_plan plan = fftwf_plan_dft_r2c_1d(FFTSIZE, fft_in, fft_out, FFTW_PATIENT);

  SF_INFO src_info = { 0, 0, 0, 0, 0, 0 };
  SNDFILE *src = sf_open(argv[1], SFM_READ, &src_info);
  float *src_audio = fftwf_malloc(sizeof(*src_audio) * CHANNELS * src_info.frames);
  sf_readf_float(src, src_audio, src_info.frames);
  sf_close(src);

  SF_INFO in_info = { 0, 0, 0, 0, 0, 0 };
  SNDFILE *in = sf_open(argv[2], SFM_READ, &in_info);
  float *in_audio = fftwf_malloc(sizeof(*in_audio) * CHANNELS * in_info.frames);
  sf_readf_float(in, in_audio, in_info.frames);
  sf_close(in);

  int max_count = src_info.frames / (FFTSIZE / OVERLAP);
  float *metrics = fftwf_malloc(sizeof(*metrics) * OCTAVES * max_count);
  float *p = metrics;
  int count = 0;
  for (int t = 0; t + FFTSIZE <= src_info.frames; t += FFTSIZE / OVERLAP) {
    // window to mono
    for (int i = t; i < t + FFTSIZE; ++i) {
      fft_in[i - t] = window[i - t] * (src_audio[CHANNELS * i + 0] + src_audio[CHANNELS * i + 1]);
    }
    fftwf_execute(plan);
    // compute metric
    for (int i = 1; i < FFTSIZE; i <<= 1) {
      float sum = 0;
      for (int k = i; k < (i << 1); ++k) {
        sum += cnormf(fft_out[k]);
      }
      *p++ = sum;
    }
    count++;
  }

  float *out_audio = fftwf_malloc(sizeof(*out_audio) * CHANNELS * in_info.frames);
  memset(out_audio, 0, sizeof(*out_audio) * CHANNELS * in_info.frames);
  for (int t = 0; t + FFTSIZE <= in_info.frames; t += FFTSIZE / OVERLAP) {
    // window to mono
    for (int i = t; i < t + FFTSIZE; ++i) {
      fft_in[i - t] = window[i - t] * (in_audio[CHANNELS * i + 0] + in_audio[CHANNELS * i + 1]);
    }
    fftwf_execute(plan);
    // compute metric
    float current[OCTAVES];
    p = current;
    int count2 = 0;
    for (int i = 1; i < FFTSIZE; i <<= 1) {
      float sum = 0;
      for (int k = i; k < (i << 1); ++k) {
        sum += cnormf(fft_out[k]);
      }
      *p++ = sum;
      count2++;
    }
    // find best match
    float minsum = 1.0f / 0.0f;
    int mini = 0;
    p = metrics;
    for (int i = 0; i < count; ++i) {
      float sum = 0;
      for (int j = 0; j < count2; ++j) {
        float d = current[j] - *p++;
        sum += d * d;
      }
      if (sum < minsum) {
        minsum = sum;
        mini = i;
      }
    }
    // regurgitate with windowing
    for (int i = 0; i < FFTSIZE; ++i) {
      for (int c = 0; c < CHANNELS; ++c) {
        out_audio[CHANNELS * (t + i) + c] +=
          window[i] * src_audio[CHANNELS * (mini * FFTSIZE / OVERLAP + i) + c];
      }
    }
  }

  // write output
  SF_INFO out_info = { 0, src_info.samplerate, CHANNELS, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0 ,0 };
  SNDFILE *out = sf_open(argv[3], SFM_WRITE, &out_info);
  sf_writef_float(out, out_audio, in_info.frames);
  sf_close(out);

  return 0;
}
