#include <complex.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fftw3.h>
#include <jack/jack.h>

#define twopi 6.283185307179586

#define SR 48000
#define CHANNELS 2
#define CHUNKSIZE 2048
#define OVERLAP 8
#define HOPSIZE (CHUNKSIZE / OVERLAP)
#define DURATION (SR * 60 * 5)


inline float cnormf(float _Complex f) {
  return crealf(f) * crealf(f) + cimagf(f) * cimagf(f);
}

struct {
  float *fft_in;
  float _Complex *fft_out;
  fftwf_plan plan;
  float *window;
  int alength;
  float *audio[CHANNELS];
  int input_index;
  float output[CHANNELS][CHUNKSIZE];
  int output_index;
  int mcount;
  int bcount;
  float *metric[CHANNELS];
  float *novelty[CHANNELS];
  float novelty_cost;
  float novelty_decay;
  jack_client_t *client;
  jack_port_t *iport[CHANNELS];
  jack_port_t *oport[CHANNELS];
} S;

void perform()
{
  int bindex = (S.input_index - CHUNKSIZE) / HOPSIZE;
  for (int c = 0; c < CHANNELS; ++c) {
    // analyse block
    for (int j = 0; j < CHUNKSIZE; ++j) {
      S.fft_in[j] = S.window[j] * S.audio[c][S.input_index - CHUNKSIZE + j];
    }
    fftwf_execute(S.plan);
    float *p = S.metric[c] + bindex * S.mcount;
    for (int j = 1; j < CHUNKSIZE >> 1; j <<= 1) {
      float sum = 0;
      for (int k = j; k < (j << 1); ++k) {
        sum += cnormf(S.fft_out[k]);
      }
      *p++ = sum;
    }
  }
  for (int c = 0; c < CHANNELS; ++c) {
    // find best matching block
    int c2 = (c + 1) % CHANNELS;
    float *p = S.metric[c2];
    float minsum = 1.0f / 0.0f;
    float mingain = 0.0f;
    int minj = 0;
    for (int j = 0; j < S.bcount; ++j) {
      float *q = S.metric[c] + bindex * S.mcount;
      float sum = 0;
      float sump = 0;
      float sumq = 0;
      for (int k = 0; k < S.mcount; ++k) {
        sump += *p * *p;
        sumq += *q * *q;
        float d = *p++ - *q++;
        sum += d * d;
      }
      float csum = (sum * sum) / (sump * sumq) + S.novelty[c2][j];
      if (csum < minsum) {
        minsum = csum;
        mingain = (1e-20f + sumq) / (1e-20f + sump);
        minj = j;
      }
    }
    mingain = fminf(10.0f, sqrtf(sqrtf(mingain)));
    // resynthesize output
    for (int j = 0; j < CHUNKSIZE; ++j) {
      S.output[c][j] += mingain * S.window[j] * S.audio[c2][minj * HOPSIZE + j];
    }
    // increment novelty
    S.novelty[c2][minj] += S.novelty_cost;
  }
  // decay novelty
  for (int c = 0; c < CHANNELS; ++c) {
    for (int j = 0; j < S.bcount; ++j) {
      S.novelty[c][j] *= S.novelty_decay;
    }
  }
  // wrap audio buffer, copying end portion back to start
  if (S.input_index == S.alength) {
    S.input_index = CHUNKSIZE - HOPSIZE;
    for (int c = 0; c < CHANNELS; ++c) {
      for (int j = 0; j < S.input_index; ++j) {
        S.audio[c][j] = S.audio[c][S.alength - S.input_index + j];
      }
      // clear metric
      for (int j = 0; j < S.mcount; ++j) {
        S.metric[c][j] = 0;
      }
    }
  }
  // shift output buffer
  for (int c = 0; c < CHANNELS; ++c) {
    for (int j = 0; j < CHUNKSIZE; ++j) {
      if (j + HOPSIZE < CHUNKSIZE) {
        S.output[c][j] = S.output[c][j + HOPSIZE];
      } else {
        S.output[c][j] = 0;
      }
    }
  }
  S.output_index = 0;
}

void process_sample(const float in[CHANNELS], float out[CHANNELS])
{
  for (int c = 0; c < CHANNELS; ++c)
  {
    S.audio[c][S.input_index] = in[c];
    out[c] = S.output[c][S.output_index];
  }
  S.input_index++;
  S.output_index++;
  if ((S.input_index & (HOPSIZE - 1)) == 0)
  {
    perform();
  }
}

int process_callback(jack_nframes_t jnframes, void *arg) {
  (void) arg;
  int nframes = jnframes;
  float *in[CHANNELS];
  float *out[CHANNELS];
  for (int c = 0; c < CHANNELS; ++c) {
    in [c] = (jack_default_audio_sample_t *) jack_port_get_buffer(S.iport[c], nframes);
    out[c] = (jack_default_audio_sample_t *) jack_port_get_buffer(S.oport[c], nframes);
  }
  for (int i = 0; i < nframes; ++i) {
    float in1[CHANNELS];
    float out1[CHANNELS];
    for (int c = 0; c < CHANNELS; ++c) {
      in1[c] = in[c] ? in[c][i] : 0;
    }
    process_sample(in1, out1);
    for (int c = 0; c < CHANNELS; ++c) {
      if (out[c]) {
        out[c][i] = out1[c];
      }
    }
  }
  return 0;
}

/* JACK error callback */
void error_callback(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

/* JACK shutdown callback */
void shutdown_callback(void *arg) {
  (void) arg;
  exit(1);
}

/* exit callback */
void atexit_callback(void) {
  jack_client_close(S.client);
}

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;

  memset(&S, 0, sizeof(S));

  S.fft_in = fftwf_malloc(sizeof(*S.fft_in) * CHUNKSIZE);
  S.fft_out = fftwf_malloc(sizeof(*S.fft_out) * CHUNKSIZE * 2);
  S.plan = fftwf_plan_dft_r2c_1d(CHUNKSIZE, S.fft_in, S.fft_out, FFTW_PATIENT);

  S.window = fftwf_malloc(sizeof(*S.window) * CHUNKSIZE);
  for (int i = 0; i < CHUNKSIZE; ++i) {
    S.window[i] = 0.5 * (1 - cos(i * twopi / CHUNKSIZE));
  }
  float gain = 0;
  for (int i = 0; i < OVERLAP; ++i)
  {
    gain += S.window[i * HOPSIZE];
  }
  for (int i = 0; i < CHUNKSIZE; ++i) {
    S.window[i] /= gain;
  }

  S.alength = (DURATION / HOPSIZE) * HOPSIZE;
  for (int c = 0; c < CHANNELS; ++c) {
    S.audio[c] = fftwf_malloc(sizeof(*S.audio) * S.alength);
    memset(S.audio[c], 0, sizeof(*S.audio) * S.alength);
  }

  S.mcount = 0;
  for (int j = 1; j < CHUNKSIZE >> 1; j <<= 1) {
    S.mcount++;
  }
  S.bcount = (S.alength - CHUNKSIZE + HOPSIZE) / HOPSIZE;
  for (int c = 0; c < CHANNELS; ++c) {
    S.metric[c] = fftwf_malloc(sizeof(*S.metric) * S.bcount * S.mcount);
    memset(S.metric[c], 0, sizeof(*S.metric) * S.bcount * S.mcount);
  }

  for (int c = 0; c < CHANNELS; ++c) {
    S.novelty[c] = fftwf_malloc(sizeof(*S.novelty) * S.bcount);
    memset(S.novelty[c], 0, sizeof(*S.novelty) * S.bcount);
  }
  S.novelty_cost = 1;
  S.novelty_decay = 0.9;

  S.input_index = CHUNKSIZE - HOPSIZE;
  S.output_index = 0;

  if (!(S.client = jack_client_open("recoda", 0, 0))) {
    fprintf (stderr, "jack server not running?\n");
    return 1;
  }
  atexit(atexit_callback);
  jack_set_process_callback(S.client, process_callback, 0);
  jack_on_shutdown(S.client, shutdown_callback, 0);

  for (int c = 0; c < CHANNELS; ++c) {
    char name[90];
    snprintf(name, 100, "input_%d", c + 1);
    S.iport[c] = jack_port_register(S.client, name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    snprintf(name, 100, "output_%d", c + 1);
    S.oport[c] = jack_port_register(S.client, name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  }
  if (jack_activate(S.client)) {
    fprintf(stderr, "cannot activate JACK client");
    return 1;
  }

  // must be activated before connecting JACK ports
  const char **ports;
  if ((ports = jack_get_ports(S.client, NULL, NULL, JackPortIsPhysical | JackPortIsInput))) {
    /* connect up to two physical playback ports */
    int i = 0;
    while (ports[i] && i < 2) {
      if (jack_connect(S.client, jack_port_name(S.oport[i]), ports[i])) {
        fprintf(stderr, "cannot connect playback port\n");
      }
      i++;
    }
    free(ports);
  }
  if ((ports = jack_get_ports(S.client, NULL, NULL, JackPortIsPhysical | JackPortIsOutput))) {
    /* connect up to two physical capture ports */
    int i = 0;
    while (ports[i] && i < 2) {
      if (jack_connect(S.client, ports[i], jack_port_name(S.iport[i]))) {
        fprintf(stderr, "cannot connect capture port\n");
      }
      i++;
    }
    free(ports);
  }

  while (1) {
    sleep(60);
  }

  return 0;
}
