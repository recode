/*
  recode -- scramble videos in time and space
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "recode-video.h"

static int match_shist(struct shist *shs, int count, struct shist *sh) {
  int mi_i = -1;
  float mi_d = 1.0f / 0.0f;
  for (int i = 0; i < count; ++i) {
    float d = 0;
    int b = 0;
    int bi = 0;
    while (b < BUCKETS || bi < BUCKETS) {
      if (b < BUCKETS && bi < BUCKETS) {
        if (shs[i].b[bi].bid == sh->b[b].bid) {
          d += powf(shs[i].b[bi++].pop - sh->b[b++].pop, 2);
        } else if (shs[i].b[bi].bid < sh->b[b].bid) {
          d += powf(shs[i].b[bi++].pop, 2);
        } else /* (shs[i].b[bi].bid > sh->b[b].bid) */ {
          d += powf(sh->b[b++].pop, 2);
        }
      } else {
        while (bi < BUCKETS) {
          d += powf(shs[i].b[bi++].pop, 2);
        }
        while (b < BUCKETS) {
          d += powf(sh->b[b++].pop, 2);
        }
      }
    }
    if (d < mi_d) {
      mi_i = i;
      mi_d = d;
    }
  }
  return mi_i;
}

int main(int argc, char **argv) {

  if (argc < 5) { return 1; }

  int width = atoi(argv[2]);
  int height = atoi(argv[3]);

  int cell = 20;
  int cols = width  / cell;
  int rows = height / cell;
  int *grid = malloc(cols * rows * sizeof(int));

  unsigned char *rgb = malloc(width * height * 3);
  struct histogram *hh = malloc(rows * sizeof(struct histogram));
  struct shist *sh = malloc(rows * sizeof(struct shist));

  struct stat st;
  stat(argv[4], &st);
  int count = st.st_size / sizeof(struct shist);
  struct shist *shs = malloc(count * sizeof(struct shist));
  FILE *slog = fopen(argv[4], "rb");
  if (1 != fread(shs, count * sizeof(struct shist), 1, slog)) { return 1; }
  fclose(slog);

  FILE *ppm = open_video(argv[1]);
  int f = 0;
  while (read_video(ppm, rgb, width, height)) {
    printf("%08d\n", f);
    // tile histograms
    {
      int i;
      #pragma omp parallel for private(i) schedule(static, 1)
      for (int j = 0; j < rows; ++j) {
        for (i = 0; i < cols; ++i) {
          calculate_shist(&sh[j], &hh[j], rgb, width, height, i * cell, j * cell, cell, cell);
          grid[j * cols + i] = match_shist(shs, count, &sh[j]);
        }
      }
    }
    for (int j = 0; j < rows; ++j) {
      for (int i = 0; i < cols; ++i) {
        printf("thumbs/%08d.ppm\n", grid[j * cols + i]);
      }
    }
  }
  close_video(ppm);

  free(rgb);
  free(shs);
  free(sh);
  free(hh);
  return 0;
}
