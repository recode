/*
  recode -- scramble videos in time and space
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#define BINS 10
typedef float histogram[BINS];

static const float infinity = 1.0f / 0.0f;

int main(int argc, char **argv) {
  if (argc < 2) { return 1; }

  struct stat st;
  stat(argv[1], &st);
  int frames = st.st_size / sizeof(histogram);
  histogram *hist = malloc(frames * sizeof(histogram));
  FILE *hf = fopen(argv[1], "rb");
  if (1 != fread(hist, frames * sizeof(histogram), 1, hf)) { return 1; }
  fclose(hf);

  for (int f0 = 0; f0 < frames; ++f0) {
    int mf = -1;
    float ms = infinity;
    for (int f = 0; f < frames; ++f) {
      if (f0 == f) { continue; }
      float s = 0;
      for (int b = 0; b < BINS; ++b) {
        float d = hist[f0][b] - hist[f][b];
        s += d * d;
      }
      float df = f0 - f;
      df *= df;
      df *= df;
      s *= 1 + frames / df;
      if (s < ms) {
        mf = f;
        ms = s;
      }
    }
    printf("%d\n", mf);
  }
  return 0;
}
