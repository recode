/*
  recode -- scramble videos in time and space
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RECODE_VIDEO_H
#define RECODE_VIDEO_H 1

#include <stdint.h>
#include <stdio.h>

#define BUCKETS 16

struct bucket {
  float pop;
  uint32_t bid;
};

struct shist {
  struct bucket b[BUCKETS];
};

struct histogram {
  struct bucket b[12][6][8];
};

void calculate_shist(struct shist *sh, struct histogram *hh, unsigned char *rgb, int rgb_w, int rgb_h, int sh_x, int sh_y, int sh_w, int sh_h);

FILE *open_video(const char *vfile);
int read_video(FILE *video, unsigned char *rgb, int width, int height);
void close_video(FILE *video);

#endif
