#!/bin/bash
# recode -- scramble videos in time and space
# Copyright (C) 2011 Claude Heiland-Allen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
T0="$(date --iso=s)"
R="$(dirname "$(readlink -f "${0}")")"
w=640
h=360
mkdir thumbs slices temp
"${R}/recode-video-pass-1" "${1}" "${w}" "${h}" "temp/frames.hist" &&
"${R}/recode-video-pass-2" "${1}" "${w}" "${h}" "temp/frames.hist" |
xargs -n 577 |
while read f ts
do
  echo $ts | xargs -n 32 pnmcat -lr | pnmsplit - slices/%d 2>/dev/null
  cd slices
  ls -1 | sort -n | xargs -n 18 pnmcat -tb
  cd ..
done |
ppmtoy4m -S444 -F25:1 |
ffmpeg -f yuv4mpegpipe -i - -vcodec libx264 -vpre lossless_medium temp/video.mkv &&
ffmpeg -i "${1}" -vn temp/audio.wav &&
ecasound -f:s16_le,2,48000 -i:resample-hq,auto,temp/audio.wav -o:temp/in.wav &&
pd -path . -stderr -r 48000 -batch -open "${R}/recode-audio.pd" 2>&1 |
grep "^RECODE:" | sed "s|^RECODE:||g" | tr " " "\n" |
"${R}/recode-audio-pass-1" temp/audio.hist &&
"${R}/recode-audio-pass-2" temp/audio.hist |
"${R}/recode-audio-pass-3" temp/in.wav temp/out.wav &&
ecasound -i:temp/out.wav -o:temp/out2.wav &&
flac --best --verify temp/out2.wav -o temp/audio.flac &&
mkvmerge -o "$(basename "${1}")-RECODED.mkv" temp/video.mkv temp/audio.flac &&
ffmpeg -i "$(basename "${1}")-RECODED.mkv" -target pal-dvd "$(basename "${1}")-RECODED.mpeg"
T1="$(date --iso=s)"
echo -e "\n\n\n${T0} START\n${T1} END"
