/*
  recode -- scramble videos in time and space
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _POSIX_C_SOURCE 2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "recode-video.h"

int main(int argc, char **argv) {
  if (argc < 5) { return 1; }

  const char *thfmt = "convert ppm:- -geometry 60x60 'thumbs/%08d.ppm'";
  int thlen = strlen(thfmt) + 8;
  char *thcmd = malloc(thlen);

  int width = atoi(argv[2]);
  int height = atoi(argv[3]);
  int crop = (width - height) >> 1;

  unsigned char *rgb = malloc(width * height * 3);
  struct histogram *hh = malloc(sizeof(struct histogram));
  struct shist *sh = malloc(sizeof(struct shist));
  FILE *slog = fopen(argv[4], "wb");

  FILE *ppm = open_video(argv[1]);
  int f = 0;
  while (read_video(ppm, rgb, width, height)) {
    // whole center histogram
    calculate_shist(sh, hh, rgb, width, height, crop, 0, height, height);
    if (1 != fwrite(sh, sizeof(struct shist), 1, slog)) { return 1; }
    // output thumbnail
    snprintf(thcmd, thlen, thfmt, f++);
    FILE *thumb = popen(thcmd, "w");
    fprintf(thumb, "P6\n%d %d 255\n", height, height);
    fflush(thumb);
    for (int j = 0; j < height; ++j) {
      if (1 != fwrite(rgb + crop * 3 + j * width * 3, height * 3, 1, thumb)) { return 1; };
    }
    pclose(thumb);
  }
  close_video(ppm);

  fclose(slog);
  free(rgb);
  free(hh);
  free(sh);
  free(thcmd);
  return 0;
}
