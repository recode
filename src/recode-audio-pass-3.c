/*
  recode -- scramble videos in time and space
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

static float   window[2048];
static float   blocks[4][2048][2];
static int16_t buffer[2048][2];
static int     which;
static int16_t output[512][2];

static const double pi = 3.141592653589793;

int main(int argc, char **argv) {
  if (argc < 3) { return 1; }
  for (int k = 0; k < 2048; ++k) {
    window[k] = 0.25 * (1.0 - cos(k * 2 * pi / 2047.0));
  }
  memset(blocks, 0, 4 * 2048 * 2 * sizeof(float));
  memset(buffer, 0, 2048 * 2 * sizeof(int16_t));
  memset(output, 0, 512 * 2 * sizeof(int16_t));
  which = 0;
  struct stat st;
  stat(argv[1], &st);
  int infilesize = st.st_size;  
  FILE *i = fopen(argv[1], "rb");
  FILE *o = fopen(argv[2], "wb");
  int l = 0;
  const unsigned char h[44] = {'R','I','F','F', 0,0,0,0, 'W','A','V','E','f','m','t',' ', 0x10,0,0,0, 1,0,2,0, 0x80,0xbb,0,0, 0,0xee,2,0, 4,0,0x10,0, 'd','a','t','a', 0,0,0,0 };
  if (1 != fwrite(h, 44, 1, o)) { return 1; }
  int f;
  while (1 == scanf("%d\n", &f)) {
    if (0 <= f && f * 512 * 2 * ((int) sizeof(int16_t)) + 44 + 2048 * 2 * ((int) sizeof(int16_t)) <= infilesize) {
      if (0 != fseek(i, f * 512 * 2 * sizeof(int16_t) + 44, SEEK_SET)) { fprintf(stderr, "seek\n"); return 1; }
      if (1 != fread(buffer, 2048 * 2 * sizeof(int16_t), 1, i)) { fprintf(stderr, "read\n"); return 1; }
      for (int k = 0; k < 2048; ++k) {
        for (int c = 0; c < 2; ++c) {
          blocks[which][k][c] = window[k] * buffer[k][c];
        }
      }
    } else {
      memset(blocks[which], 0, 2048 * 2 * sizeof(float));
    }
    for (int k = 0; k < 512; ++k) {
      for (int c = 0; c < 2; ++c) {
        float o = 0;
        for (int b = which; b < which + 4; ++b) {
          int d = b - which;
          o += blocks[b % 4][512 * d + k][c];
        }
        output[k][c] = fminf(fmaxf(o, -32767), 32767);
      }
    }
    if (1 != fwrite(output, 512 * 2 * sizeof(int16_t), 1, o)) { fprintf(stderr, "write\n"); return 1; }
    l += 512 * 2 * sizeof(int16_t);
    which = (which - 1 + 4) % 4;
  }
  fclose(i);
  if (0 != fseek(o, 4, SEEK_SET)) { fprintf(stderr, "seek'\n"); return 1; }
  unsigned char w[4];
  int m = l + 36;
  w[3] = (m >> 24) & 0xFF;
  w[2] = (m >> 16) & 0xFF;
  w[1] = (m >>  8) & 0xFF;
  w[0] = (m      ) & 0xFF;
  if (1 != fwrite(w, 4, 1, o)) { fprintf(stderr, "write'\n"); return 1; }
  if (0 != fseek(o, 40, SEEK_SET)) { fprintf(stderr, "seek''\n"); return 1; }
  w[3] = (l >> 24) & 0xFF;
  w[2] = (l >> 16) & 0xFF;
  w[1] = (l >>  8) & 0xFF;
  w[0] = (l      ) & 0xFF;
  if (1 != fwrite(w, 4, 1, o)) { fprintf(stderr, "write''\n"); return 1; }
  fclose(o);
  return 0;
}
