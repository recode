/*
  recode -- scramble videos in time and space
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _POSIX_C_SOURCE 2

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "recode-video.h"

static int bpopcmp(const void *x, const void *y) {
  const struct bucket *a = x;
  const struct bucket *b = y;
  if (a->pop >  b->pop) { return -1; }
  if (a->pop <  b->pop) { return  1; }
  return 0;  
}

static int bbidcmp(const void *x, const void *y) {
  const struct bucket *a = x;
  const struct bucket *b = y;
  if (a->bid <  b->bid) { return -1; }
  if (a->bid >  b->bid) { return  1; }
  return 0;  
}

static int min(int a, int b) { return a < b ? a : b; }
static int max(int a, int b) { return a > b ? a : b; }

void calculate_shist(struct shist *sh, struct histogram *hh, unsigned char *rgb, int rgb_w, int rgb_h, int sh_x, int sh_y, int sh_w, int sh_h) {
  int bid = 0;
  for (int i = 0; i < 12; ++i) {
    for (int j = 0; j < 6; ++j) {
      for (int k = 0; k < 8; ++k) {
        hh->b[i][j][k].pop = 0.0f;
        hh->b[i][j][k].bid = bid++;
      }
    }
  }
  for (int y = sh_y; y < sh_y + sh_h; ++y) {
    for (int x = sh_x; x < sh_x + sh_w; ++x) {
      unsigned char *p = rgb + 3 * rgb_w * y + 3 * x;
      float r = *p++ / 255.0f;
      float g = *p++ / 255.0f;
      float b = *p   / 255.0f;
      // rgb to hsv
      float mi = fminf(fminf(r, g), b);
      float ma = fmaxf(fmaxf(r, g), b);
      float h = 0; float s = 0; float v = ma;
      if (v > 0) {
        r /= v; g /= v; b /= v; mi /= v; ma /= v; s = ma - mi;
        if (s > 0) {
          r = (r - mi) / s; g = (g - mi) / s; b = (b - mi) / s;
          mi = fminf(fminf(r, g), b); ma = fmaxf(fmaxf(r, g), b);
          if (ma == r) { h = 2 * (g - b); if (h < 0) { h += 12; }
          } else if (ma == g) { h = 4 + 2 * (b - r);
          } else /* ma == b) */ { h = 8 + 2 * (r - g); }
        }
      }
      int bh = floor(h);            bh = min(max(0, bh), 11);
      int bs = floor(5 * s + 0.5);  bs = min(max(0, bs),  5);
      int bv = floor(7 * v + 0.5);  bv = min(max(0, bv),  7);
      hh->b[bh][bs][bv].pop += 1.0f;
    }
  }
  qsort(hh, 12 * 6 * 8, sizeof(struct bucket), bpopcmp);
  memcpy(sh, hh, sizeof(struct shist));
  qsort(sh, BUCKETS, sizeof(struct bucket), bbidcmp);
  for (int i = 0; i < BUCKETS; ++i) {
    sh->b[i].pop /= (sh_w * sh_h);
  }
}

FILE *open_video(const char *vfile) {
  const char *fmt = "ffmpeg -i '%s' -f image2pipe pipe:-.ppm";
  int len = strlen(fmt) + strlen(vfile);
  char *cmd = malloc(len);
  snprintf(cmd, len, fmt, vfile);
  return popen(cmd, "r");
}

int read_video(FILE *video, unsigned char *rgb, int width, int height) {
  char hdr[64];
  char hdr2[64];
  snprintf(hdr, 60, "P6\n%d %d\n255\n", width, height);
  if (1 == fread(hdr2, strlen(hdr), 1, video)) {
    hdr2[strlen(hdr)] = 0;
    if (strcmp(hdr, hdr2)) { return 0; }
    if (1 != fread(rgb, width * height * 3, 1, video)) { return 0; }
    return 1;
  } else {
    return 0;
  }
}

void close_video(FILE *video) {
  pclose(video);
}
